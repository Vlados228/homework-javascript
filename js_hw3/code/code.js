//Дані два масиви: ['a', 'b', 'c'] та [1, 2, 3]. Об'єднайте їх разом.

const arr1 = ['a', 'b', 'c'];
const arr2 = [1, 2, 3];
const arr3 = arr1.concat(arr2);
document.write(arr3);
document.write('<hr/>');

//Дан масив ['a', 'b', 'c']. Додайте йому до кінця елементи 1, 2, 3.

const arr4 = ['a', 'b', 'c'];
const arr5 = arr4.concat(1, 2, 3);
document.write(arr5);
document.write('<hr/>');

//Дан масив [1, 2, 3]. Зробіть із нього масив [3, 2, 1].

const arr6 = [1, 2, 3];
arr6.reverse();
document.write(arr6);
document.write('<hr/>');

//Дан масив [1, 2, 3]. Додайте йому до кінця елементи 4, 5, 6.

const arr7 = [1, 2, 3];
arr7.push(4, 5, 6)
document.write(arr7);
document.write('<hr/>');

//Дан масив [1, 2, 3]. Додайте йому на початок елементи 4, 5, 6.

const arr8 = [1, 2, 3];
arr8.unshift(4, 5, 6);
document.write(arr8);
document.write('<hr/>');

//Дан масив ['js', 'css', 'jq']. Виведіть перший елемент на екран.

const arr9 = ['js', 'css', 'jq']
document.write(arr9[0]);
document.write('<hr/>');

//Дан масив [1, 2, 3, 4, 5]. За допомогою методу slice запишіть нові елементи [1, 2, 3].

const arr10 = [1,2,3,4,5];
const arr11 = arr10.slice(0, 3);
document.write(arr11);
document.write('<hr/>');

//Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 4, 5].

const arr12 = [1,2,3,4,5];
const arr13 = arr12.splice(1, 2);
document.write(arr12);
document.write('<hr/>');

//Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 2, 10, 3, 4, 5].

const arr14 = [1,2,3,4,5];
const arr15 = arr14.splice(2,0,10);
document.write(arr14);
document.write('<hr/>');

//Дан масив [3, 4, 1, 2, 7]. Відсортуйте його

let arr16 = [3,4,1,2,7];
arr16.sort();
document.write(arr16);
document.write('<hr/>');

//Дан масив з елементами 'Привіт, ', 'світ' і '!'. Потрібно вивести на екран фразу 'Привіт, мир!'.

let arr17 = ['Привіт, ','світ','!'];
arr17[1] = ('мир');
let arr18 = arr17.join("")
document.write(arr18);
document.write('<hr/>');

//Дан масив ['Привіт, ', 'світ', '!']. Необхідно записати в нульовий елемент цього масиву слово 'Поки, ' (тобто замість слова 'Привіт, ' буде 'Поки, ').

let arr19 = ['Привіт, ','світ','!'];
arr19[0] = ('Поки, ');
let arr20 = arr19.join("")
document.write(arr20);
document.write('<hr/>');

//Створіть масив arr з елементами 1, 2, 3, 4, 5 двома різними способами.

let arr21 = [1,2,3,4,5];
let arr22 = new Array(1,2,3,4,5);
document.write(arr21 + '<br>');
document.write(arr22);
document.write('<hr/>');

/**
 * Дан багатовимірний масив arr:
        <br>
        <pre>
            <code>
                var arr = {
                    'ru':['блакитний', 'червоний', 'зелений'],
                    'en':['blue', 'red', 'green'],
                };
            </code>
        </pre>
        Виведіть за його допомогою слово 'блакитний' 'blue' .
 */

var arr23 = [
    ['блакитний', 'червоний', 'зелений'],
    ['blue', 'red', 'green'],
];
document.write(arr23[0][0] + ' ' + arr23[1][0]);
document.write('<hr/>');

//Створіть масив arr = ['a', 'b', 'c', 'd'] і за допомогою його виведіть на екран рядок 'a+b, c+d'.

let arr24 = ['a','b','c','d'];
let arr25 = arr24.splice(0,4,'a+','b, ','c+','d')
let arr26 = arr24.join('');
document.write(arr26);
document.write('<hr/>');    

/**
 * Запитайте у користувача кількість елементів масиву. Виходячи з даних, які ввів користувач
    створіть масив на ту кількість елементів, яку передав користувач.
    у кожному індексі масиву зберігайте чило який показуватиме номер елемента масиву.
 */

let gElements = parseInt(prompt("", "10"));
let elements = [];
for(let i = 1; i <= gElements; i++){
    elements.push(i)
};
document.write(elements);
document.write('<hr/>');

//Зробіть так, щоб з масиву, який ви створили вище, вивелися всі непарні числа в параграфі, а парні в спані з червоним тлом.
let pair = [];
let noteven = [];
for(let f = 0; f < elements.length; f++){
    if (elements[f] % 2 === 0){ 
        pair.push(elements[f]);        
    }else{
        noteven.push(elements[f]);
    }
}
document.write(noteven + '<p/>');
document.write(`<div class="red"> ${pair}</div>`)    
document.write('<hr/>');

/**
 * Напишіть код, який перетворює та об'єднує всі елементи масиву в одне рядкове значення. Елементи масиву будуть розділені комою.
           <pre>
               <code>
                var vegetables = ['Капуста', 'Ріпа', 'Редиска', 'Морковка'];
    
                <span class="green">// Ваш код</span>

                
                document.write(str1); // "Капуста, Ріпа, Редиска, Морквина"
               </code>
           </pre>
 */

let vegetables = ['Капуста', 'Ріпа', 'Редиска', 'Морковка'];
vegetables1 = vegetables.join(', ')
document.write(vegetables1);
document.write('<hr/>');
